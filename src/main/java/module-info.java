module com.example.obhajoba {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;


    opens com.example.obhajoba to javafx.fxml;
    exports com.example.obhajoba;
}