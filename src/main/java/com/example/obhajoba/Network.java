package com.example.obhajoba;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Network {
    private ArrayList<Subnet> subnets;
    private String networkAddress;


    public Network(ArrayList<Subnet> subnets, String networkAddress) {
        this.subnets = subnets;
        this.networkAddress = networkAddress;
    }


    private void countIps() {
        String tempAddress = networkAddress;
        for (Subnet subnet: subnets) {

            //Adresa site
            if (tempAddress.equals(networkAddress)){
                subnet.setNetworkAddress(tempAddress);
            } else {
                tempAddress = addToIp(tempAddress, 1);
                subnet.setNetworkAddress(tempAddress);
            }

            //Min IP
            tempAddress = addToIp(tempAddress, 1);
            subnet.setFirstAddress(tempAddress);

            //Max IP
            tempAddress = addToIp(tempAddress, subnet.getAllocatedSize() - 3);
            subnet.setLastAddress(tempAddress);

            //Broadcast
            tempAddress = addToIp(tempAddress, 1);
            subnet.setBroadcast(tempAddress);


        }
    }

    private ArrayList<Integer> ipToArrayList(String ip) {
        String[] strings =  ip.split("\\.");
        List<String> list = Arrays.asList(strings);
        ArrayList<Integer> ipArrayList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            ipArrayList.add(Integer.parseInt(list.get(i)));
        }

        return ipArrayList;
    }

    private String addToIp(String ip, int number) {
        ArrayList<Integer> ipArrayList = ipToArrayList(ip);
        if (ipArrayList.get(3) + number > 255){
            ipArrayList.set(2, ipArrayList.get(2) + 1);
            ipArrayList.set(3, ipArrayList.get(3) + number - 255);
        } else {
            ipArrayList.set(3, ipArrayList.get(3) + number);
        }

        return ipArrayList.get(0) + "." + ipArrayList.get(1) + "." + ipArrayList.get(2) + "." + ipArrayList.get(3);
    }


    public ObservableList<Subnet> getSubnets () {
        countIps();
        return FXCollections.observableArrayList(subnets);
    }

    public String getAddress() {
        return this.networkAddress;
    }

}
