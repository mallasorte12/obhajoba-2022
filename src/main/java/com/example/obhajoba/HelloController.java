package com.example.obhajoba;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;
import java.util.Collections;

public class HelloController {

    @FXML
    private TableView<Subnet> tableView;

    @FXML
    private TextField inputField;

    @FXML
    private PieChart pieChart = new PieChart(null);


    public void count () {
        Alert alert = initAlert();
        InputHandler ih = new InputHandler(alert);


        try {
            String[] input = inputField.getText().split("\\s+");
            ArrayList<Subnet> subnets = ih.handleInput(input);
            if (subnets.size() < 1 || checkHosts(input)) {
                alert.showAndWait();
                return;
            }
            Collections.sort(subnets);
            Network n = new Network(subnets, input[0]);
            tableInit();
            tableView.setItems(n.getSubnets());
            fillPie(n);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            alert.showAndWait();
            tableView.setVisible(false);
        }
    }


    public void tableInit () {

        TableColumn<Subnet, String> name = new TableColumn<>("Název");
        name.setMinWidth(60);
        name.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Subnet, String> neededSize = new TableColumn<>("Počet PC");
        neededSize.setMinWidth(70);
        neededSize.setCellValueFactory(new PropertyValueFactory<>("neededSize"));

        TableColumn<Subnet, String> allocatedSize = new TableColumn<>("Počet IP");
        allocatedSize.setMinWidth(70);
        allocatedSize.setCellValueFactory(new PropertyValueFactory<>("allocatedSize"));

        TableColumn<Subnet, String> mask = new TableColumn<>("Subnet Maska");
        mask.setMinWidth(115);
        mask.setCellValueFactory(new PropertyValueFactory<>("mask"));

        TableColumn<Subnet, String> networkAddress = new TableColumn<>("Adresa sítě");
        networkAddress.setMinWidth(115);
        networkAddress.setCellValueFactory(new PropertyValueFactory<>("networkAddress"));

        TableColumn<Subnet, String> firstAddress = new TableColumn<>("Min IP");
        firstAddress.setMinWidth(115);
        firstAddress.setCellValueFactory(new PropertyValueFactory<>("firstAddress"));

        TableColumn<Subnet, String> lastAddress = new TableColumn<>("Max IP");
        lastAddress.setMinWidth(115);
        lastAddress.setCellValueFactory(new PropertyValueFactory<>("lastAddress"));

        TableColumn<Subnet, String> broadcast = new TableColumn<>("Broadcast");
        broadcast.setMinWidth(115);
        broadcast.setCellValueFactory(new PropertyValueFactory<>("broadcast"));

        TableColumn<Subnet, String> prefix = new TableColumn<>("Prefix");
        prefix.setMinWidth(50);
        prefix.setCellValueFactory(new PropertyValueFactory<>("prefix"));

        tableView.getColumns().clear(); //Prevent stacking of columns while clicking the button again
        tableView.getColumns().addAll(name, neededSize, allocatedSize, mask, networkAddress, firstAddress,
                                        lastAddress, broadcast, prefix);
        tableView.setVisible(true);
    }

    public void fillPie (Network n) {
        int allocatedSize = 0;
        ObservableList<Subnet> subnets = n.getSubnets();
        ObservableList<PieChart.Data> pieData = FXCollections.observableArrayList();

        for (Subnet tempSubnet : subnets) {
            pieData.add(new PieChart.Data(tempSubnet.getName(), tempSubnet.getAllocatedSize()));
            allocatedSize += tempSubnet.getAllocatedSize();
        }

        pieData.add(new PieChart.Data("Volný prostor", 255 - allocatedSize));
        pieChart.setData(pieData);
        pieChart.setVisible(true);
    }

    public Alert initAlert () {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Chyba");
        alert.setHeaderText("Chyba!");
        alert.setContentText("Zadali jste vstup v nesprávném tvaru.");
        return alert;
    }

    public boolean checkHosts(String[] input) {
        int counter = 0;

        for (int i = 1; i < input.length; i++) {
            counter += Integer.parseInt(input[i]);
            if (Integer.parseInt(input[i]) == 0){
                return true;
            }
        }
        if (counter > 255){
            return true;
        }
        return false;
    }
}