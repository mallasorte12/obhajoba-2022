package com.example.obhajoba;

import javafx.scene.control.Alert;

import java.lang.reflect.Array;
import java.util.*;

public class InputHandler {

    private final String[] networkNames = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
            "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    private final String IPV4_REGEX =
            "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
                    "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
                    "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
                    "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";

    private final Map<String, String> prefixToMask = new HashMap<String, String>() {{
        put("24", "255.255.255.0");
        put("25", "255.255.255.128");
        put("26", "255.255.255.192");
        put("27", "255.255.255.224");
        put("28", "255.255.255.240");
        put("29", "255.255.255.248");
        put("30", "255.255.255.252");
    }};

    private final Map<String, int[]> prefixToIpRange = new HashMap<String, int[]>() {{
        put("24", new int[]{129, 256});
        put("25", new int[]{65, 128});
        put("26", new int[]{33, 64});
        put("27", new int[]{17, 32});
        put("28", new int[]{9, 16});
        put("29", new int[]{5, 8});
        put("30", new int[]{1, 4});
    }};

    private final ArrayList<Subnet> payload = new ArrayList<>();
    private Alert alert;
    private String[] hostsArray;
    public String networkAddress;

    public InputHandler (Alert alert) {
        this.alert = alert;
    }


    public ArrayList<Subnet> handleInput(String[] input) {
        String[] octetsStr = input[0].split("\\.");
        ArrayList<Integer> octetsInt = new ArrayList<Integer>();
        for (int i = 0; i < 4; i++) {
            octetsInt.add(Integer.parseInt(octetsStr[i]));
        }

        if (input[0].matches(IPV4_REGEX) && input.length > 1 && Collections.max(octetsInt) < 256){
            this.hostsArray = Arrays.copyOfRange(input, 1, input.length + 1);
            return setVariables();
        }

        return payload;
    }

    private ArrayList<Subnet> setVariables() {
        for (int i = 0; i < hostsArray.length - 1; i++) {

            payload.add(new Subnet());
            Subnet tempSubnet = payload.get(i);

            //Nazev
            tempSubnet.setName(networkNames[i]);

            //Pocet PC, Pocet IP
            int neededSize = Integer.parseInt(hostsArray[i]);

            tempSubnet.setNeededSize(neededSize);
            tempSubnet.setAllocatedSize(neededSize + 2);

            //Maska
            String prefix = getPrefixFromHosts(tempSubnet.getAllocatedSize());
            tempSubnet.setMask(prefixToMask.get(prefix));

            //Prefix, Pocet IP
            for (Map.Entry<String, int[]> entry : prefixToIpRange.entrySet()) {

                int[] range = entry.getValue();
                if (range[0] <= tempSubnet.getAllocatedSize() && tempSubnet.getAllocatedSize() <= range[1]){
                    tempSubnet.setAllocatedSize(range[1]);
                    tempSubnet.setPrefix("/" + entry.getKey());
                    break;
                }
            }
        }

        return payload;
    }

    private String getPrefixFromHosts(int hosts) {
        for (String i : prefixToIpRange.keySet()) {
            int[] range = prefixToIpRange.get(i);
            if (hosts >= range[0] && hosts <= range[1]) {
                return i;
            }
        }
        return "error";
    }

}
